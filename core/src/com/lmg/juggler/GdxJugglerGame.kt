package com.lmg.juggler

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2

class GdxJugglerGame : ApplicationAdapter() {

    private val _bgColor = Color(109f/255f, 88f/255f, 181f/255f, 1f)

    private lateinit var batch: SpriteBatch
    private lateinit var img: Texture
    private var position = Vector2(0f, 0f)

    private var _pixelsPerMeter = 1f
    private var _gravity = Vector2(0f, -9.8f)
    private var _velocity = Vector2(0f, 0f)
    private var _kickForce = 10f

    override fun create() {
        batch = SpriteBatch()
        img = Texture("soccer-ball.png")

        _pixelsPerMeter = Gdx.graphics.ppcY
        position.x = Gdx.graphics.width / 2f - img.width / 2f
        position.y = Gdx.graphics.height - img.height.toFloat()
    }

    override fun render() {
        update(delta = Gdx.graphics.deltaTime)
        draw()
    }

    override fun dispose() {
        batch.dispose()
        img.dispose()
    }

    private fun update(delta: Float) {

        if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
            val center = position.cpy().add(img.width / 2f, img.height / 2f)
            val mousePosition = Vector2(
                    Gdx.input.x.toFloat(),
                    Gdx.graphics.height - Gdx.input.y.toFloat()
            )

            if (center.dst(mousePosition) <= img.width / 2f) {
                val force = center
                        .sub(mousePosition)
                        .nor()
                        .scl(_kickForce)
                _velocity.set(force.x, _kickForce)
            }
        }

        val adjustment = _velocity
                .add(_gravity.x * delta, _gravity.y * delta)

        position.add(adjustment)

        // If Ball Hits Wall
        when {
            position.x <= 0f ||
            position.x >= Gdx.graphics.width - img.width -> _velocity.scl(-1f, 1f)
        }

        if (position.y < -img.height) {
            // Ball Fell below screen reset State
            _velocity.set(0f, 0f)
            position.set(
                    MathUtils.random(img.width / 2f, Gdx.graphics.width - img.width / 2f),
                    Gdx.graphics.height + img.height.toFloat()
            )
        }
    }

    private fun draw() {
        Gdx.gl.glClearColor(_bgColor.r, _bgColor.g, _bgColor.b, _bgColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        batch.begin()
        batch.draw(img, position.x, position.y)
        batch.end()
    }
}